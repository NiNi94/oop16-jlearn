# README #
# JLearn:  A Juicy Learning app 
JLearn is an open source Language Learning App made available under the GPLv3 license.
Copyright (C) 2017 Luca Giulianini Copyright (C) 2017 Gyordan Caminati Copyright (C) 2017 Sara Kiade.

## Overview

In the past few years speaking more than a language has became an essential skill, so people are now interested in learning languages.
This is where JLearn comes into play.
JLearn is a Language Learning Application (LLA), written in Java, which is now configured for English learning (but it can be set up to fit every exercise-based use).
The application aims at leading users through a language learning, mixing business with pleasure.  That's why it's a good looking application, which embraces Material Design by Google.

JLearn has also features which lift user experience to another level, such as Undecorated Windows style, speech recognition, webcam pictures acquisition, and maps consultation.

If you want to support our work and contribute to this project, click on the Pull Request button.

# We look towards future not past, towards style not rubbish.

##Downloads

- JLearn 1.0.1 [App](https://sourceforge.net/projects/jlearn-material/files/JLearn.jar/download)
- JLearn 1.0.1 [Javadoc](https://sourceforge.net/projects/jlearn-material/files/JLearn_1.0.0_javadoc.jar/download)
- JLearn [Paper](https://sourceforge.net/projects/jlearn-material/files/JLearn_Relazione.pdf/download)

## System Requirements

- Java 64-bit
- last JavaFX version
- RAM: minimum 4 Gb, reccomended 8 Gb or more 
- CPU: at least i5 3th, reccomended i7 or more

Headphones use is reccomended, for a better experience.


## Testing
Tested and developed on Linux Mint Sarah Kde (18), Mac OSX Sierra and Windows7/10.

#Windows
No issues.
#Mac
Pdf Encoding on Mac OSX Sierra -> sometimes do not show well. Tried to fix that without good result.
# Linux
There is a well known issue on certain Linux's Distros with JavaFX Media Player. If you encount this bug please follow this guide.

- [Media Player Fix](http://docs.oracle.com/javafx/2/system_requirements_2-2-5/jfxpub-system_requirements_2-2-5.htm)


## Resources
- Copyright free music from [Majestic Casual](https://www.youtube.com/channel/UCXIyz409s7bNWVcM-vjfdVA)
- [Kaelyn - The Mood](https://www.youtube.com/watch?v=dCPGb-UQzV4)
- [Olmos - Hold Me](https://www.youtube.com/watch?v=vf_S0-V6wTE)
- [Rare - Pure ft. Zeina](https://www.youtube.com/watch?v=ruHk8z_PoWI)
- [KAASI - Maybe Monday](https://www.youtube.com/watch?v=bSKo1b_r3L8)
- Button sounds by [Luca Giulianini](https://bitbucket.org/NiNi94/)


## Java Libraries

- [Annotation for FindBugs](_)
- [Apache Commons CSV](https://commons.apache.org/proper/commons-csv/)
- [Apache Commons I/O](https://commons.apache.org/proper/commons-io/)
- [JCDP](https://github.com/dialex/JCDP) - Java Colored Debug Printer
- [LOG4J](https://logging.apache.org/log4j/2.x/) - Log Library
- [WebCam Capture](http://webcam-capture.sarxos.pl) - WebCam Library
- [BridJ](https://github.com/nativelibs4java/BridJ) - WebCam Dependency Library
- [SL4J](https://www.slf4j.org) - WebCam Logs
- [SL4J-NOOP](https://www.slf4j.org) - WebCam Logs
- [Sphinx4](https://cmusphinx.github.io) - Voice Recognition


## JavaFX Libraries
- [ControlsFX](http://fxexperience.com/controlsfx/) - JavaFX UI Controls
- [Enzo](https://bitbucket.org/hansolo/enzo/wiki/Home) - JavaFX UI Controls
- [IKONLI](https://github.com/aalmiray/ikonli) - JavaFX Icon Library
- [IKONLI-CORE](https://github.com/aalmiray/ikonli/tree/master/subprojects/ikonli-core) - JavaFX Icon Library
- [IKONLI-MATERIAL-PACK](https://github.com/aalmiray/ikonli/tree/master/subprojects/ikonli-material-pack) - JavaFX Icon Material Pack
- [JFXtras](http://jfxtras.org) - JavaFX UI extra Controls
- [Medusa](https://github.com/HanSolo/Medusa) - JavaFX UI Gauge
- [GUAVA](https://github.com/google/guava) - Google Core Libraries for Java
- [JavaFX](http://docs.oracle.com/javase/8/javafx/get-started-tutorial/jfx-overview.htm#JFXST784)
- [JFoenix](https://github.com/jfoenixadmin/JFoenix) - Material Design for JavaFX
- [GMaps](http://rterp.github.io/GMapsFX/) - GoogleMaps for JavaFX

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Developers

- Luca Giulianini (IT) : 

    Part: View

    e-mail: luca.giulianini2@studio.unibo.it

- Sara Kiade (IT)


    Part: Model  

    
    e-mail: sara.kiade@studio.unibo.it

- Jordan Caminati (IT)
    
    
    Part: Controller
    
    
    e-mail: gyordan.caminati@studio.unibo.it


    

Here are some screenshots:

![picture](pics/menu.png)
![picture](pics/player.png)
![picture](pics/signIn.png)
![picture](pics/chooseUser.png)
![picture](pics/groupPhoto.png)
![picture](pics/home.png)
![picture](pics/theoryPDF.png)
![picture](pics/maps.png)
![picture](pics/completeEx.png)
![picture](pics/tfEx.png)
![picture](pics/multiEx.png)
![picture](pics/statsHome.png)
![picture](pics/statsBar.png)
![picture](pics/statsCake.png)
![picture](pics/statsLine.png)
![picture](pics/Popup.png)




